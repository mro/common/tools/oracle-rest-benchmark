/*
 * NTOFException.cpp
 *
 *  Created on: Oct 13, 2014
 *      Author: mdonze
 */

#include "NTOFException.h"

#include <sstream>

namespace ntof {

/**
 * Built exception using a string
 */
NTOFException::NTOFException(const std::string &Msg,
                             const std::string &file,
                             int Line,
                             int error) :
    what_(),
    msg(Msg),
    file_(file),
    line(Line),
    errorCode(error)
{}

/**
 * Build exception using a char* and an error code
 */
NTOFException::NTOFException(const char *Msg,
                             const std::string &file,
                             int Line,
                             int error) :
    what_(),
    msg(Msg),
    file_(file),
    line(Line),
    errorCode(error)
{}

/**
 * Empty constructor
 */
NTOFException::NTOFException() : what_(), msg(), file_(), line(0), errorCode(0)
{}

/**
 * Gets error message
 */
const char *NTOFException::what() const noexcept
{
    if (what_.empty())
    {
        std::ostringstream oss;
        oss << "NTOF exception in source file " << file_ << " at line " << line
            << " : " << msg;
        what_ = oss.str();
    }
    return what_.c_str();
}

} // namespace ntof
