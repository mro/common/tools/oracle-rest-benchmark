//
// Created by matteof on 1/15/20.
//

#include "CurlHttpCaller.hpp"

#include <iostream>
#include <memory>
#include <utility>

#include <json/json.h>

namespace
{

std::size_t callback(
    const char* in,
    std::size_t size,
    std::size_t num,
    std::string* out)
{
    const std::size_t totalBytes(size * num);
    out->append(in, totalBytes);
    return totalBytes;
}

}

CurlHttpCallerException::CurlHttpCallerException(std::string message) : msg(std::move(message)) {}
CurlHttpCallerException::CurlHttpCallerException(const char *message) : msg(message) {}
CurlHttpCallerException::~CurlHttpCallerException() throw() = default;
/**
 * Overload of std::exception
 * @return Error message
 */
const char * CurlHttpCallerException::what() const noexcept
{
    return msg.c_str();
}
/**
 * Gets the error message
 * @return The error message
 */
const char * CurlHttpCallerException::getMessage() const noexcept
{
    return msg.c_str();
}


Json::Value CurlHttpCaller::operator()() const {
    CURL* curl = curl_;

    // Set remote URL.
    curl_easy_setopt(curl, CURLOPT_URL, url_.c_str());
    // Don't bother trying IPv6, which would increase DNS resolution time.
    curl_easy_setopt(curl, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4);
    // Don't wait forever, time out after 10 seconds.
    curl_easy_setopt(curl, CURLOPT_TIMEOUT, 10);
    // Follow HTTP redirects if necessary.
    curl_easy_setopt(curl, CURLOPT_FOLLOWLOCATION, 1L);
    // Response information.
    long httpCode(0);
    std::unique_ptr<std::string> httpData(new std::string());
    // Hook up data handling function.
    curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, callback);
    // Hook up data container (will be passed as the last parameter to the
    // callback handling function).  Can be any pointer type, since it will
    // internally be passed as a void pointer.
    curl_easy_setopt(curl, CURLOPT_WRITEDATA, httpData.get());
    // Run our HTTP GET command, capture the HTTP response code, and clean up.
    curl_easy_perform(curl);
    curl_easy_getinfo(curl, CURLINFO_RESPONSE_CODE, &httpCode);

    Json::Value jsonData;
    if (httpCode == 200 && parse(*httpData, jsonData))
    {
        return jsonData;
    }
    else
    {
        throw CurlHttpCallerException("Error during HTTP GET Call. Couldn't GET from " + url_);
    }
}

bool CurlHttpCaller::parse(const std::string &iValueStr, Json::Value& oValueJson) const
{
    Json::Reader jsonReader;
    if (jsonReader.parse(iValueStr, oValueJson))
    {
        return true;
    }
    else
    {
        std::cout << "Could not parse HTTP data as JSON" << std::endl;
        std::cout << "HTTP data was:\n" << iValueStr << std::endl;
        return false;
    }
}
