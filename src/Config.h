//
// Created by matteof on 01/07/2020.
//

#ifndef CONFIG_H
#define CONFIG_H

#include <vector>

#include <sys/types.h>
#include <Singleton.hxx>

namespace ntof {
namespace bench {

struct BenchTest {
    std::string testName;
    std::string oracleQuery;
    std::string restQuery;

    std::string getOracleTestName() const
    {
        return "Oracle-" + testName;
    }

    std::string getRestTestName() const
    {
        return "Rest-" + testName;
    }

};

struct BenchOptions {
    uint32_t repetitions;
    uint32_t threads;
    uint32_t iterations;

    BenchOptions(): repetitions(1), threads(1), iterations(1) {}
};

class Config : public ntof::utils::Singleton<Config>
{
public:
    static std::string configFile_;     //!< Path of the config file

    /*
     * \brief Destructor of the class
     */
    virtual ~Config() = default;

    /**
     * @brief load configuration from the given files
     * @param[in] file the configuration file to load
     *
     * @details this method will destroy any existing Config and instantiate
     * the mutex again
     */
    static Config &load(const std::string &file = configFile_);

    const std::string &getOracleUser() const;
    void setOracleUser(const std::string &oracleUser);
    const std::string &getOraclePass() const;
    void setOraclePass(const std::string &oraclePass);
    const std::string &getOracleServer() const;
    void setOracleServer(const std::string &oracleServer);
    const std::string &getRestServer() const;
    void setRestServer(const std::string &restServer);
    const std::vector<BenchTest> &getBenchTests() const;
    const BenchOptions &getBenchOptions() const;

protected:
    friend class ntof::utils::Singleton<Config>;

    explicit Config(const std::string &file = configFile_);

    // Oracle nTOF Database credentials can be overridden
    std::vector<BenchTest> benchTests_;
    BenchOptions benchOptions_;
    std::string oracleUser_;
    std::string oraclePass_;
    std::string oracleServer_;
    std::string restServer_;
};

} // namespace bench
} // namespace ntof

#endif // CONFIG_H
