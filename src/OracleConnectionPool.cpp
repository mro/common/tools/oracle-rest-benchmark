//
// Created by matteof on 1/16/20.
//

#include "OracleConnectionPool.hpp"

#include <easylogging++.h>

template class ntof::utils::Singleton<ntof::bench::OracleConnectionPool>;

namespace ntof {
namespace bench {

/**
 * Fail-over callback for Oracle
 * @param
 * @param
 * @param
 * @param foType
 * @param foEvent
 * @return
 */
int taf_callback(oracle::occi::Environment *,
                 oracle::occi::Connection *,
                 void *,
                 oracle::occi::Connection::FailOverType foType,
                 oracle::occi::Connection::FailOverEventType foEvent)
{
    switch (foEvent)
    {
    case oracle::occi::Connection::FO_END: break;
    case oracle::occi::Connection::FO_ABORT: break;
    case oracle::occi::Connection::FO_REAUTH: break;
    case oracle::occi::Connection::FO_BEGIN: break;
    case oracle::occi::Connection::FO_ERROR: break;
    default: break;
    }

    switch (foType)
    {
    case oracle::occi::Connection::FO_NONE: break;
    case oracle::occi::Connection::FO_SESSION: break;
    case oracle::occi::Connection::FO_SELECT: break;
    default: break;
    }
    LOG(INFO) << "Got Oracle fail-over callback Type : " << foType
              << " event : " << foEvent;
    if (foEvent == oracle::occi::Connection::FO_ERROR)
        return OCI_FO_RETRY;
    if (foEvent == oracle::occi::Connection::FO_END)
    {
        return 0;
    }
    return 0;
}


OracleConnectionPool::OracleConnectionPool(const std::string &dbServer,
                                           const std::string &dbUser,
                                           const std::string &dbPass)
{
    oracleUser_ = dbUser;
    oraclePass_ = dbPass;
    oracleServer_ = dbServer;

    LOG(INFO) << "Creating Oracle environment";
    dbEnv_ = oracle::occi::Environment::createEnvironment(
        oracle::occi::Environment::THREADED_MUTEXED);
    LOG(INFO)
        << "Connecting to Oracle database : " << oracleServer_
        << " Username : " << oracleUser_;
    dbConPool_ =
        dbEnv_->createStatelessConnectionPool(oracleUser_,
            oraclePass_,oracleServer_, 10);
}

OracleConnectionPool::~OracleConnectionPool()
{
    LOG(INFO) << "Destroying OracleConnectionPool object";
    destroyConnection();
}

/**
 * Destroy all Oracle connections
 */
void OracleConnectionPool::destroyConnection()
{
    // Close connection if possible
    try
    {
        if ((dbConPool_ != NULL) && (dbEnv_ != NULL))
        {
            LOG(INFO) << "Calling DBEnv terminateStatelessConnectionPool";
            dbEnv_->terminateStatelessConnectionPool(dbConPool_);
            dbConPool_ = NULL;
        }
    }
    catch (...)
    {}

    // Destroy environment if possible
    try
    {
        if (dbEnv_ != NULL)
        {
            oracle::occi::Environment::terminateEnvironment(dbEnv_);
            dbEnv_ = NULL;
        }
    }
    catch (...)
    {}
}

OracleConnectionPool& OracleConnectionPool::initialize(std::string dbServer,
                                                      std::string dbUser,
                                                      std::string dbPass)
{
    ntof::utils::SingletonMutex lock;

    if (m_instance)
    {
        delete m_instance;
        m_instance = nullptr;
    }

    m_instance = new OracleConnectionPool(std::move(dbServer), dbUser, dbPass);
    return *m_instance;
}

/**
 * Helper to create a self-destructable statement
 */
OracleConnectionPool::Statement::Statement()
{
    dbCon_ = OracleConnectionPool::instance().dbConPool_->getAnyTaggedConnection();
    dbCon_->setTAFNotify(taf_callback, NULL);
    stmt_ = dbCon_->createStatement();
    stmt_->setAutoCommit(true);
}

OracleConnectionPool::Statement::Statement(const std::string &sql)
{
    dbCon_ = OracleConnectionPool::instance().dbConPool_->getAnyTaggedConnection();
    dbCon_->setTAFNotify(taf_callback, NULL);
    stmt_ = dbCon_->createStatement(sql);
    stmt_->setAutoCommit(true);
}

OracleConnectionPool::Statement::~Statement()
{
    if (dbCon_)
    {
        if (stmt_)
        {
            dbCon_->terminateStatement(stmt_);
        }
        OracleConnectionPool::instance().dbConPool_->releaseConnection(dbCon_);
    }
}

oracle::occi::Statement *OracleConnectionPool::Statement::get()
{
    return stmt_;
}

oracle::occi::Statement *OracleConnectionPool::Statement::operator->() const
{
    return stmt_;
}

} // namespace bench
} // namespace ntof
