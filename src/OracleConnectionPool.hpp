//
// Created by matteof on 1/16/20.
//

#ifndef ORACLECONNECTIONPOOL_HPP
#define ORACLECONNECTIONPOOL_HPP

#include <occi.h>
#include <Singleton.hxx>

namespace ntof {
namespace bench {

class OracleConnectionPool: public ntof::utils::Singleton<OracleConnectionPool>
{
public:

    virtual ~OracleConnectionPool();

    static OracleConnectionPool& initialize(std::string dbServer,
                                           std::string dbUser,
                                           std::string dbPass);

    class Statement
    {
    public:
        Statement();
        explicit Statement(const std::string &sql);
        virtual ~Statement();
        oracle::occi::Statement *get();
        oracle::occi::Statement *operator->() const;

    private:
        oracle::occi::Connection *dbCon_;
        oracle::occi::Statement *stmt_;
    };

protected:
    friend class ntof::utils::Singleton<OracleConnectionPool>;
    explicit OracleConnectionPool(const std::string &dbServer = "server",
                                  const std::string &dbUser = "user",
                                  const std::string &dbPass = "pass");
    void destroyConnection();


    oracle::occi::Environment *dbEnv_;
    oracle::occi::StatelessConnectionPool *dbConPool_;
    std::string oracleUser_;
    std::string oraclePass_;
    std::string oracleServer_;

};

#endif // ORACLECONNECTIONPOOL_HPP

} // namespace bench
} // namespace ntof