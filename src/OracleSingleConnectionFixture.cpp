//
// Created by matteof on 1/9/20.
//

#include "OracleSingleConnectionFixture.hpp"

#include <benchmark/benchmark.h>

void OracleSingleConnectionFixture::SetUp(::benchmark::State& state) {
    Config &conf = Config::instance();
    dbEnv_ = Environment::createEnvironment(Environment::THREADED_MUTEXED);
    dbCon_ = dbEnv_->createConnection(
        conf.getOracleUser(), conf.getOraclePass(), conf.getOracleServer());
    int a = 2;
}

void OracleSingleConnectionFixture::TearDown(::benchmark::State& state) {
    dbEnv_->terminateConnection(dbCon_);
    Environment::terminateEnvironment(dbEnv_);
    dbCon_ = nullptr;
    dbEnv_ = nullptr;
}