#include "Config.h"
#include "CurlHttpCaller.hpp"
#include "NTOFException.h"
#include "OracleConnectionPool.hpp"
// #include "OracleSingleConnectionFixture.hpp"

// Include logging stuff
#include <easylogging++.h>
#include <cxxopts.hpp>
#include <benchmark/benchmark.h>
#include <json/json.h>
#include <stdlib.h>

_INITIALIZE_EASYLOGGINGPP

using namespace ntof::bench;

auto PerformOracleQuery = [](benchmark::State& state, std::string sql)
    {
      for (auto _ : state) {
          OracleConnectionPool::Statement stmt(sql);
          oracle::occi::ResultSet *rs = stmt->executeQuery();
          if (rs->next() != oracle::occi::ResultSet::DATA_AVAILABLE)
          {
              stmt->closeResultSet(rs);
              std::ostringstream oss;
              oss << "Nothing found in the db not found in database!";
              LOG(ERROR) << "Nothing found in the db not found in database!";
              throw ntof::NTOFException(oss.str(), __FILE__, __LINE__);
          }
          else
          {
              //Just fetch the whole ResultSet
              while(rs->next())
              {
                  //TODO access all the columns?
              }
          }
          stmt->closeResultSet(rs);
      }
    };

auto PerformRestQuery = [](benchmark::State& state, std::string rest)
    {
      CurlHttpCaller caller(rest);
      for (auto _ : state) {
          Json::Value res = caller();
      }
    };

int main(int argc, char **argv)
{
    try
    {
        _START_EASYLOGGINGPP(argc, argv);

        cxxopts::Options options("OracleRestApiBench",
 "This software is executing benchmark on Oracle query and same query through REST API. ");
        options
            .allow_unrecognised_options()
            .add_options()
                ("help", "Print help")
                ("c,config", "config XML file",
                 cxxopts::value<std::string>())
                ("u,user", "Oracle user",
                 cxxopts::value<std::string>())
                ("p,password", "Oracle password",
                 cxxopts::value<std::string>())
                ("s,server", "Oracle server",
                 cxxopts::value<std::string>())
                ("r,rest-server", "Rest-API server",
                 cxxopts::value<std::string>());
                /*("benchmark_out", "Output file name",
                 cxxopts::value<std::string>()->default_value("benchmark.json"))
                ("benchmark_out_format", "Output file format (csv, json)",
                 cxxopts::value<std::string>()->default_value("json"));*/

        auto result = options.parse(argc, argv);

        std::string configStr = Config::configFile_;
        if(result.count("config"))
        {
            configStr = result["config"].as<std::string>();
        }
        // std::string configStr = "/home/matteof/workspace/oracle-rest-api-benchmark/xml/confBench.xml";
        Config::load(configStr);
        Config& config = Config::instance();
        if(result.count("user"))
        {
            // Override oracle user
            config.setOracleUser(result["user"].as<std::string>());
        }
        if(result.count("password"))
        {
            // Override oracle password
            config.setOraclePass(result["password"].as<std::string>());
        }
        if(result.count("server"))
        {
            // Override oracle server
            config.setOracleServer(result["server"].as<std::string>());
        }
        if(result.count("rest-server"))
        {
            // Override rest-api server
            config.setRestServer(result["rest-server"].as<std::string>());
        }
        OracleConnectionPool::initialize(config.getOracleServer(), config.getOracleUser(), config.getOraclePass());

        LOG(INFO) << "Registering benchmark tests";

        //Register all tests
        for (const BenchTest& bTest : config.getBenchTests())
        {
            LOG(INFO) << "Registering --> " << bTest.testName;
            ::benchmark::RegisterBenchmark(bTest.getOracleTestName().c_str(), PerformOracleQuery, bTest.oracleQuery)
                ->Unit(benchmark::kMillisecond)
                ->Threads(config.getBenchOptions().threads)
                ->Repetitions(config.getBenchOptions().repetitions)
                ->Iterations(config.getBenchOptions().iterations);
            ::benchmark::RegisterBenchmark(bTest.getRestTestName().c_str(), PerformRestQuery, bTest.restQuery)
                ->Unit(benchmark::kMillisecond)
                ->Threads(config.getBenchOptions().threads)
                ->Repetitions(config.getBenchOptions().repetitions)
                ->Iterations(config.getBenchOptions().iterations);
        }

        LOG(INFO) << "Benchmark is starting ";

        ::benchmark::Initialize(&argc, argv);
        if (::benchmark::ReportUnrecognizedArguments(argc, argv)) return 1;
        ::benchmark::RunSpecifiedBenchmarks();

        OracleConnectionPool::destroy();
        Config::destroy();
    }
    catch (const ntof::NTOFException &ex)
    {
        LOG(ERROR)
            << "[FATAL ERROR] Caught ntof exception : " << ex.getMessage();
    }
    catch (const std::exception &ex)
    {
        LOG(ERROR) << "[FATAL ERROR] Caught exception : " << ex.what();
    }
    catch (...)
    {
        LOG(ERROR) << "[FATAL ERROR] Caught unknown exception : ";
    }

    return 0;
}


/*
BENCHMARK_DEFINE_F(OracleSingleConnectionFixture, BasicTest)(benchmark::State& state) {
    for (auto _ : state)
    {
        Statement *stmt = dbCon_->createStatement("SELECT * FROM test WHERE name='Matteo'");
        ResultSet *rs = stmt->executeQuery();
        stmt->closeResultSet(rs);
        dbCon_->terminateStatement(stmt);
    }
}

BENCHMARK_REGISTER_F(OracleSingleConnectionFixture, BasicTest)->Unit(benchmark::kMillisecond)->Threads(1)->Repetitions(1)->Iterations(5);


static void TestHTTPGetCaller(benchmark::State& state)
{
    if (state.thread_index == 0)
    {
        // Prepare
    }
    while (state.KeepRunning())
    {
        CurlHttpCaller caller("http://localhost:8080/db/runs/item/64");
        Json::Value res = caller();
    }
    if (state.thread_index == 0)
    {
        // Teardown
    }
}

BENCHMARK(TestHTTPGetCaller)->Unit(benchmark::kMillisecond)->Threads(1)->Repetitions(1)->Iterations(5);
*/
