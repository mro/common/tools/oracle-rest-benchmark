//
// Created by matteof on 1/9/20.
//

#ifndef ORACLESINGLECONNECTIONFIXTURE_HPP
#define ORACLESINGLECONNECTIONFIXTURE_HPP

#include <occi.h>
#include <benchmark/benchmark.h>
#include "Config.h"

using namespace oracle::occi;
using namespace ntof::bench;

class OracleSingleConnectionFixture : public benchmark::Fixture {
protected:
    Environment *dbEnv_;
    Connection *dbCon_;

public:
    void SetUp(::benchmark::State& state) override;
    void TearDown(::benchmark::State& state) override;
};

#endif // ORACLESINGLECONNECTIONFIXTURE_HPP
