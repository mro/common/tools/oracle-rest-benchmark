//
// Created by matteof on 01/07/2020.
//

#include "Config.h"
#include "NTOFException.h"

#include <easylogging++.h>
#include <json/json.h>
#include <iostream>
#include <fstream>

#define THROW_ERROR(msg)                                        \
{                                                               \
    std::ostringstream oss;                                     \
    oss << msg;                                                 \
    LOG(ERROR) << oss.str();                                    \
    throw NTOFException(oss.str(), __FILE__, __LINE__);         \
}


template class ntof::utils::Singleton<ntof::bench::Config>;

namespace ntof {
namespace bench {

std::string Config::configFile_ = "/root/config/confBench.xml";

Config::Config(const std::string &file)
{
    Json::Value root;
    Json::CharReaderBuilder builder;
    std::ifstream test(file, std::ifstream::binary);
    std::string errs;

    if ( !Json::parseFromStream(builder, test, &root, &errs) )
    {
        THROW_ERROR("Unable to open and parse configuration file " << file
                          << "| Reason: " << errs);
    }

    const Json::Value oracle = root["oracle"];
    if(oracle.isNull())
    {
        THROW_ERROR("Oracle credentials are missing!");
    }
    oracleUser_ = oracle["user"].asString();
    oraclePass_ = oracle["password"].asString();
    oracleServer_ = oracle["server"].asString();

    const Json::Value rest = root["rest"];
    if(rest.isNull())
    {
        THROW_ERROR("Rest info are missing!");
    }
    restServer_ =  rest.get("server", "http://localhost:8080").asString();

    const Json::Value benchOptions = root["benchmark-options"];
    if(benchOptions.isNull())
    {
        THROW_ERROR("Benchmark opions are missing!");
    }
    benchOptions_.iterations = benchOptions.get("iterations", 1).asUInt();
    benchOptions_.repetitions = benchOptions.get("repetitions", 1).asUInt();
    benchOptions_.threads = benchOptions.get("threads", 1).asUInt();

    const Json::Value tests = root["tests"];
    if(tests.isNull() || tests.size() <= 0)
    {
        THROW_ERROR("Tests are missing!");
    }
    for (Json::Value::ArrayIndex i = 0; i != tests.size(); i++ )
    {
        BenchTest benchTest;
        benchTest.testName = tests[i]["title"].asString();
        benchTest.oracleQuery = tests[i]["oracleQuery"].asString();
        benchTest.restQuery = getRestServer() + tests[i]["restQuery"].asString();
        benchTests_.push_back(benchTest);
    }

    // Print config
    LOG(INFO) << "=== Application settings ===";
    LOG(INFO) << "Oracle User : " << getOracleUser();
    LOG(INFO) << "Oracle Pass : " << getOraclePass();
    LOG(INFO) << "Oracle Server : " << getOracleServer();

    LOG(INFO) << "Rest Server : " << getRestServer();

    LOG(INFO) << "Option Iterations : " << getBenchOptions().iterations;
    LOG(INFO) << "Option Repetitions : " << getBenchOptions().repetitions;
    LOG(INFO) << "Option Threads : " << getBenchOptions().threads;

    LOG(INFO) << "Loaded " << benchTests_.size() << " tests from " << file;
}

Config &Config::load(const std::string &file)
{
    ntof::utils::SingletonMutex lock;

    if (m_instance)
    {
        delete m_instance;
        m_instance = nullptr;
    }

    m_instance = new Config(file);
    return *m_instance;
}

const std::string &Config::getOracleUser() const
{
    return oracleUser_;
}

const std::string &Config::getOraclePass() const
{
    return oraclePass_;
}

const std::string &Config::getOracleServer() const
{
    return oracleServer_;
}

const std::string &Config::getRestServer() const
{
    return restServer_;
}

const std::vector<BenchTest> &ntof::bench::Config::getBenchTests() const
{
    return benchTests_;
}
const BenchOptions &Config::getBenchOptions() const
{
    return benchOptions_;
}
void Config::setOracleUser(const std::string &oracleUser)
{
    oracleUser_ = oracleUser;
}
void Config::setOraclePass(const std::string &oraclePass)
{
    oraclePass_ = oraclePass;
}
void Config::setOracleServer(const std::string &oracleServer)
{
    oracleServer_ = oracleServer;
}
void Config::setRestServer(const std::string &restServer)
{
    restServer_ = restServer;
}

} // namespace bench
} // namespace ntof