//
// Created by matteof on 1/15/20.
//

#include <string>

#include <curl/curl.h>

// Forward Declaration.
namespace Json {
class Value;
}

class CurlHttpCallerException : public std::exception
{
public:
    explicit CurlHttpCallerException(std::string message);
    explicit CurlHttpCallerException(const char *message);
    virtual ~CurlHttpCallerException() throw();
    /**
     * Overload of std::exception
     * @return Error message
     */
    virtual const char *what() const throw();

    /**
     * Gets the error message
     * @return The error message
     */
    const char *getMessage() const throw();

private:
    std::string msg; //!< Error message
};


#ifndef CURLHTTPCALLER_HPP
#define CURLHTTPCALLER_HPP

class CurlHttpCaller
{
public:
    explicit CurlHttpCaller(std::string iUrl): url_(iUrl), curl_(curl_easy_init()) {};
    ~CurlHttpCaller() { curl_easy_cleanup(curl_); }
    Json::Value operator()() const;

private:
    std::string url_;
    CURL* curl_;

    bool parse(const std::string &iValueStr, Json::Value &oValueJson) const;
};

#endif // CURLHTTPCALLER_HPP
