FROM cern/cc7-base

WORKDIR /app
ADD . /app
RUN yum update -y && \
    yum install -y cmake3 make gcc-c++ git && \
    yum-config-manager --add-repo "https://apc-dev.web.cern.ch/distfiles/cc7/APC-Common.repo" && \
    yum install -y oracle-instantclient19.5-devel && \
    yum install -y jsoncpp jsoncpp-devel && \
    yum install -y libcurl libcurl-devel && \

    yum install -y 'devtoolset-7-gcc*' && \
    source /opt/rh/devtoolset-7/enable && \
    g++ --version | grep -q '(GCC) 7' && \
    git submodule update --init --recursive && \
    rm -rf build && mkdir build && cd build && \
    cmake3 .. -DCMAKE_BUILD_TYPE=Release && \
    make -j4 && \
    cd .. && \

    curl -sL https://rpm.nodesource.com/setup_12.x | bash - && \
    yum install -y nodejs && \

    cd www && \
    npm install && \
    npm install pm2 && rm -rf build && mkdir .pm2 && chmod 777 .pm2 && \
    echo 'Startup Done.'
ENV PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:www/node_modules/.bin
ENV HOME=/app

CMD pm2 --no-daemon start -o /dev/null -e /dev/null --no-autorestart ./www/index.js
EXPOSE 8080/tcp
