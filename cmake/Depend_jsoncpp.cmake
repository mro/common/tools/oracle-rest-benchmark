
include(Tools)

find_package(PkgConfig REQUIRED)
pkg_check_modules(JSONCPP jsoncpp)

assert(JSONCPP_INCLUDE_DIRS AND JSONCPP_LIBRARIES MESSAGE "Failed to find JSONCPP")
