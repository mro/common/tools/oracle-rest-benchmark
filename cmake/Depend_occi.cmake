
function(glob_dir name path)
  file(TO_CMAKE_PATH "${path}" path)
  file(GLOB dir LIST_DIRECTORIES true "${path}")
  if(dir)
    list(APPEND ${name} "${dir}")
    set(${name} "${${name}}" PARENT_SCOPE)
  endif()
endfunction()

set(_paths)
glob_dir(_paths "/usr/local/oracle*/")
glob_dir(_paths "/usr/include/oracle/*/")

find_path(Oracle_INCLUDE_DIRS
    NAMES occi.h
    HINTS $ENV{Oracle_DIR} $ENV{ORACLE_HOME}
    PATHS ${_paths}
    PATH_SUFFIXES include client64 sdk/include
    CMAKE_FIND_ROOT_PATH_BOTH NO_DEFAULT_PATH)

set(_paths "")
glob_dir(_paths "/usr/local/oracle*/")
glob_dir(_paths "/usr/lib/oracle/*/")
if (Oracle_INCLUDE_DIRS MATCHES "usr[/]+include[/]+(oracle[/]+.*)")
  list(APPEND _paths "/usr/lib/${CMAKE_MATCH_1}")
endif ()

find_library(OCIEI_LIBRARY
  NAMES ociei
  HINTS $ENV{Oracle_DIR} $ENV{ORACLE_HOME}
  PATHS ${_paths}
  PATH_SUFFIXES lib sdk/lib
  CMAKE_FIND_ROOT_PATH_BOTH
  NO_DEFAULT_PATH)

include(Tools)

assert(OCIEI_LIBRARY MESSAGE "Oracle library not found")
assert(Oracle_INCLUDE_DIRS MESSAGE "Oracle includes not found")

# RPM package has no symlink for occi library
string(REGEX REPLACE "ociei.*" "occi*" occi_file "${OCIEI_LIBRARY}")
file(GLOB OCCI_LIBRARY "${occi_file}")
assert(OCCI_LIBRARY MESSAGE "Oracle occi library not found")

set(Oracle_LIBRARIES ${OCIEI_LIBRARY} ${OCCI_LIBRARY} CACHE PATH "" FORCE)
