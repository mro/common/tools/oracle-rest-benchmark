/**
 * Required External Modules
 */

const express = require("express");
const path = require("path");

/**
 * App Variables
 */

const app = express();
const port = process.env.PORT || "8080";

/**
 *  App Configuration
 */
app.engine('pug', require('pug').__express);
app.set("views", path.join(__dirname, "views"));
app.set("view engine", "pug");
app.use(express.static(path.join(__dirname, "public")));
// JS library
app.use('/highcharts', express.static(__dirname + '/node_modules/highcharts'));
app.use('/bootstrap/js', express.static(__dirname + '/node_modules/bootstrap/dist/js'));
app.use('/popper.js/js', express.static(__dirname + '/node_modules/popper.js/dist/umd'));
app.use('/jquery/js', express.static(__dirname + '/node_modules/jquery/dist'));
app.use('/lodash', express.static(__dirname + '/node_modules/lodash'));
// CSS
app.use('/bootstrap/css', express.static(__dirname + '/node_modules/bootstrap/dist/css'));

/**
 * Routes Definitions
 */

app.get("/", (req, res) => {
    res.render("index", {});
});

app.get('/api/benchmark', (req, res) => {
  let jsonObj = require('./data/BenchmarkResults.json');
  res.json(jsonObj);
});

/**
 * Server Activation
 */

app.listen(port, () => {
    console.log(`Benchmark results are ready on http://localhost:${port}`);
});