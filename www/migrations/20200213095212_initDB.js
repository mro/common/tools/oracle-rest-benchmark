
exports.up = async function(knex) {
  await knex.schema.createTable('OPERATION_PERIODS',
  (table) => {
    table.integer('OPER_YEAR').primary();
    table.timestamp('OPER_START').notNull();
    table.timestamp('OPER_STOP').notNull();
  });

  await knex.schema.createTable('EXPERIMENTAL_AREAS',
  (table) => {
    table.integer('EAR_NUMBER').primary();
    table.string('EAR_NAME', 25).unique().notNull();
  });

  await knex.schema.createTable('MATERIALS_TYPE',
    (table) => {
    table.string('MAT_TYPE', 20).primary();
  });

  await knex.schema.createTable('MATERIALS',
    (table) => {
      table.increments('MAT_ID');
      table.string('MAT_TYPE', 20).notNull()
        .references('MATERIALS_TYPE.MAT_TYPE').withKeyName('MAT_TYPE_FK');
      table.string('MAT_TITLE', 100).notNull();
      table.string('MAT_COMPOUND', 25).notNull();
      table.integer('MAT_MASS_NUMBER').notNull().comment('0=natural element, -1=compound');
      table.float('MAT_MASS_MG', 126);
      table.integer('MAT_ATOMIC_NUMBER');
      table.float('MAT_THICKNESS_UM', 126);
      table.float('MAT_DIAMETER_MM', 126);
      table.integer('MAT_STATUS').notNull();
      table.float('MAT_AREA_DENSITY_UM_CM2', 126);
      table.string('MAT_DESCRIPTION', 4000).notNull();

      table.index(['MAT_TYPE', 'MAT_COMPOUND'], 'MAT_TYPE_COMPOUND_IDX');
    });

  await knex.schema.createTable('MATERIALS_SETUPS',
    (table) => {
      table.increments('MAT_SETUP_ID');
      table.string('MAT_SETUP_NAME', 255).unique();
      table.string('MAT_SETUP_DESCRIPTION', 255);
      table.integer('MAT_SETUP_EAR_NUMBER').notNull()
        .references('EXPERIMENTAL_AREAS.EAR_NUMBER').withKeyName('MAT_SETUP_EAR_NUMBER_FK');
      table.integer('MAT_SETUP_OPER_YEAR').notNull()
        .references('OPERATION_PERIODS.OPER_YEAR').withKeyName('MAT_SETUP_OPER_YEAR_FK');

      table.index(['MAT_SETUP_EAR_NUMBER', 'MAT_SETUP_OPER_YEAR'], 'MAT_EAR_YEAR_IDX');
    });

    await knex.schema.createTable('DOCUMENTS',
      (table) => {
        table.increments('DOC_ID');
        table.string('DOC_TITLE', 255).notNull();
        table.string('DOC_FILE_NAME', 100).notNull();
        table.integer('DOC_SIZE').notNull();
        table.binary('DOC_DOCUMENT').notNull();
      });

    await knex.schema.createTable('DETECTORS',
      (table) => {
        table.increments('DET_ID');
        table.string('DET_NAME', 50).notNull().unique();
        table.string('DET_DESCRIPTION', 255).notNull();
        table.string('DET_DAQNAME', 255);
        table.string('DET_TRECNUMBER', 255);

        table.index(['DET_DAQNAME'], 'DET_DAQNAME_IDX');
      });

    await knex.schema.createTable('DETECTORS_SETUPS',
      (table) => {
        table.increments('DET_SETUP_ID');
        table.string('DET_SETUP_NAME', 255).unique();
        table.string('DET_SETUP_DESCRIPTION', 255);
        table.integer('DET_SETUP_EAR_NUMBER').notNull()
          .references('EXPERIMENTAL_AREAS.EAR_NUMBER').withKeyName('DET_SETUP_EAR_NUMBER_FK');
        table.integer('DET_SETUP_OPER_YEAR').notNull()
          .references('OPERATION_PERIODS.OPER_YEAR').withKeyName('DET_SETUP_OPER_YEAR_FK');

        table.index(['DET_SETUP_EAR_NUMBER', 'DET_SETUP_OPER_YEAR', 'DET_SETUP_NAME'], 'DET_SETUP_EAR_YEAR_NAME_IDX');
     });

    await knex.schema.createTable('CONTAINERS',
      (table) => {
        table.increments('CONT_ID');
        table.string('CONT_NAME', 255).notNull();
        table.string('CONT_DESCRIPTION', 255);
        table.string('CONT_TRECNUMBER', 255);

        table.index(['CONT_NAME'], 'CONT_NAME_IDX');
     });

    await knex.schema.createTable('SHIFT_KINDS',
      (table) => {
        table.string('SHK_KIND', 35).primary();
      });

    await knex.schema.createTable('POINTS_SHIFTS',
      (table) => {
        table.increments('POINT_ID');
        table.integer('POINT_DAY_OF_WEEK').notNull().comment('POSIX day of week (0=Sunday)');
        table.float('POINT_VALUE', 126).notNull();
        table.integer('POINT_OPER_YEAR').notNull()
          .references('OPERATION_PERIODS.OPER_YEAR').withKeyName('POINT_OPER_YEAR_FK');
        table.string('POINT_SHK_KIND', 35).notNull()
          .references('SHIFT_KINDS.SHK_KIND').withKeyName('POINT_SHK_KIND_FK');

        table.unique(['POINT_OPER_YEAR', 'POINT_DAY_OF_WEEK', 'POINT_SHK_KIND'], 'POINT_YEAR_WEEK_KIND_UNIQ');
      });

    await knex.schema.createTable('USERS',
      (table) => {
        table.increments('USER_ID');
        table.string('USER_LOGIN', 20).unique().comment('may be null for expired users');
        table.string('USER_FIRST_NAME', 50).notNull();
        table.string('USER_LAST_NAME', 50).notNull();
        table.string('USER_EMAIL', 50).notNull();
        table.integer('USER_STATUS').notNull();
        table.integer('USER_PRIVILEGES').notNull();

        table.index(['USER_LAST_NAME', 'USER_FIRST_NAME'], 'USER_NAME_IDX');
      });

    await knex.schema.createTable('INSTITUTES',
      (table) => {
        table.increments('INST_ID');
        table.string('INST_NAME', 255).notNull();
        table.string('INST_CITY', 50).comment('no notNull constraint to migrate old data');
        table.string('INST_COUNTRY', 50).notNull();
        table.string('INST_ALIAS', 50).unique().notNull();
        table.integer('INST_STATUS').notNull().comment('1 for active, 0 for inactive');

        table.index(['INST_STATUS', 'INST_NAME'], 'INST_STATUS_NAME_IDX');
      });

    await knex.schema.createTable('ASSOCIATIONS',
      (table) => {
        table.increments('ASSO_ID');
        table.integer('ASSO_OPER_YEAR').notNull()
          .references('OPERATION_PERIODS.OPER_YEAR').withKeyName('ASSO_OPER_YEAR_FK');
        table.integer('ASSO_USER_ID').notNull()
          .references('USERS.USER_ID').withKeyName('ASSO_USER_ID_FK');
        table.integer('ASSO_INST_ID').notNull()
          .references('INSTITUTES.INST_ID').withKeyName('ASSO_INST_ID_FK');

        table.unique(['ASSO_OPER_YEAR', 'ASSO_USER_ID'], 'ASSO_OPER_USER_UNIQ');
        table.index('ASSO_USER_ID', 'ASSO_USER_IDX');
        table.index(['ASSO_OPER_YEAR', 'ASSO_INST_ID'], 'ASSO_YEAR_INST_IDX');
      });

    await knex.schema.createTable('SHIFTS',
      (table) => {
        table.increments('SHIFT_ID');
        table.timestamp('SHIFT_DATE').notNull();
        table.integer('SHIFT_ASSO_ID').notNull()
          .references('ASSOCIATIONS.ASSO_ID').withKeyName('SHIFT_ASSO_ID_FK');
        table.string('SHIFT_SHK_KIND', 35).notNull()
          .references('SHIFT_KINDS.SHK_KIND').withKeyName('SHIFT_SHK_KIND_FK');

        table.unique(['SHIFT_DATE', 'SHIFT_SHK_KIND'], 'SHIFT_DATE_KIND_UNIQ');
        table.index(['SHIFT_DATE', 'SHIFT_ASSO_ID'], 'SHIFT_DATE_ASSO_IDX');
      });

    await knex.schema.createTable('RUN_STATUS',
      (table) => {
        table.string('RUN_STATUS', 12).primary();
      });

    await knex.schema.createTable('RUN_DATA_STATUS',
      (table) => {
        table.string('RUN_DATA_STATUS', 12).primary();
      });

    await knex.schema.createTable('RUNS',
      (table) => {
      table.increments('RUN_ID');
      table.timestamp('RUN_START').notNull();
      table.timestamp('RUN_STOP');
      table.string('RUN_TITLE', 255).notNull();
      table.string('RUN_DESCRIPTION', 4000);
      table.string('RUN_TITLE_ORIGINAL', 255);
      table.string('RUN_DESCRIPTION_ORIGINAL', 4000);
      table.integer('RUN_RUN_NUMBER').unique().notNull();
      table.string('RUN_CASTOR_FOLDER', 255).notNull();
      table.integer('RUN_TOTAL_PROTONS').defaultTo(0);
      table.string('RUN_RUN_STATUS', 12).notNull()
        .references('RUN_STATUS.RUN_STATUS').withKeyName('RUNS_RUN_STATUS_FK');

      table.string('RUN_DATA_STATUS', 12).notNull()
        .references('RUN_DATA_STATUS.RUN_DATA_STATUS').withKeyName('RUNS_RUN_DATA_STATUS_FK');

      table.integer('RUN_DET_SETUP_ID').notNull()
        .references('DETECTORS_SETUPS.DET_SETUP_ID').withKeyName('RUNS_DET_SETUP_ID_FK');

      table.integer('RUN_MAT_SETUP_ID').notNull()
        .references('MATERIALS_SETUPS.MAT_SETUP_ID').withKeyName('RUNS_MAT_SETUP_ID_FK');

      table.index(['RUN_START'], 'RUN_START_IDX');
      table.index(['RUN_TITLE'], 'RUN_TITLE_IDX');
    });

    if (knex.client.dialect === 'oracledb') {
      await knex.raw( 'CREATE TRIGGER "RUNS_BEFORE_INSERT_TRIG" BEFORE INSERT ON "RUNS" ' +
                      'FOR EACH ROW BEGIN ' +
                      '  :NEW.RUN_TITLE_ORIGINAL := :NEW.RUN_TITLE;' +
                      '  :NEW.RUN_DESCRIPTION_ORIGINAL := :NEW.RUN_DESCRIPTION;' +
                      'END;'
      );
    }

    await knex.schema.createTable('COMMENTS',
      (table) => {
        table.increments('COM_ID');
        table.timestamp('COM_DATE').unique().notNull();
        table.string('COM_COMMENTS', 4000);
        table.integer('COM_RUN_ID').notNull()
          .references('RUNS.RUN_ID').withKeyName('COMMENTS_RUN_ID_FK');
        table.integer('COM_USER_ID').notNull()
          .references('USERS.USER_ID').withKeyName('COMMENTS_USER_ID_FK');

        table.index(['COM_RUN_ID', 'COM_DATE'], 'COM_RUN_DATE_IDX');
      });

    await knex.schema.createTable('HV_PARAMETERS',
      (table) => {
        table.increments('HV_ID');
        table.integer('HV_SLOT_ID').notNull();
        table.integer('HV_SERIAL_NUMBER').comment('null on old entries');
        table.integer('HV_CHANNEL_ID').notNull();
        table.string('HV_NAME', 255).notNull();
        table.float('HV_VO_SET_VOLT', 126).notNull();
        table.float('HV_IO_SET_AMP', 126).notNull();
        table.float('HV_TRIP', 126).notNull();
        table.float('HV_RAMP_UP', 126).notNull();
        table.float('HV_RAMP_DOWN', 126).notNull();
        table.integer('HV_V_MAX').notNull();
        table.integer('HV_RUN_ID').notNull()
          .references('RUNS.RUN_ID').withKeyName('HV_PARAMETERS_RUN_ID_FK');
        table.unique(['HV_RUN_ID', 'HV_CHANNEL_ID', 'HV_SLOT_ID'], 'HV_RUN_CHAN_SLOT_UNIQ');

        table.index(['HV_RUN_ID'], 'HV_RUN_IDX');
      });

    await knex.schema.createTable('DAQ_INSTRUMENTS',
      (table) => {
        table.increments('DAQI_ID');
        table.integer('DAQI_CHANNEL').notNull();
        table.integer('DAQI_MODULE').notNull();
        table.integer('DAQI_CRATE').notNull();
        table.integer('DAQI_STREAM').notNull();
        table.unique(['DAQI_STREAM', 'DAQI_CRATE', 'DAQI_MODULE', 'DAQI_CHANNEL'], 'DAQ_INSTRUMENTS_UNIQ');
      });

    await knex.schema.createTable('DAQ_PARAMETERS',
      (table) => {
        table.increments('PARAM_ID');
        table.integer('PARAM_INDEX').notNull();
        table.string('PARAM_NAME', 50).notNull();
        table.string('PARAM_VALUE', 50);
        table.unique(['PARAM_INDEX', 'PARAM_NAME', 'PARAM_VALUE'], 'DAQ_PARAMETERS_UNIQ');
      });

    await knex.schema.createTable('CONFIGURATIONS',
      (table) => {
        table.increments('CONF_ID');
        table.integer('CONF_STATUS').notNull();
        table.integer('CONF_RUN_ID').notNull()
          .references('RUNS.RUN_ID').withKeyName('CONFIGURATIONS_RUN_ID_FK');
        table.integer('CONF_DAQI_ID').notNull()
          .references('DAQ_INSTRUMENTS.DAQI_ID').withKeyName('CONFIGURATIONS_DAQI_ID_FK');
        table.unique(['CONF_RUN_ID', 'CONF_DAQI_ID'], 'CONFIGURATIONS_RUN_DAQI_UNIQ');

        table.index(['CONF_RUN_ID'], 'CONF_RUN_ID_IDX');
      });

    await knex.schema.createTable('TRIGGERS',
      (table) => {
        table.increments('TRIG_ID');
        table.string('TRIG_SOURCE', 55).notNull();
        table.timestamp('TRIG_CYCLESTAMP');
        table.integer('TRIG_TRIGGER_NUMBER').notNull();
        table.float('TRIG_INTENSITY', 126);
        table.integer('TRIG_RUN_ID').notNull()
          .references('RUNS.RUN_ID').withKeyName('TRIGGERS_RUN_ID_FK');

        // FIXME: temporarily removing, calibration triggers have identical number and stamp
        // table.unique([ 'TRIG_RUN_ID', 'TRIG_TRIGGER_NUMBER'  ], 'TRIGGERS_RUN_NUMBER_UNIQ');
        table.index(['TRIG_RUN_ID', 'TRIG_TRIGGER_NUMBER'], 'TRIGGERS_RUN_NUMBER_IDX');
      });

    await knex.schema.createTable('CASTOR_FILES',
      (table) => {
        table.increments('CF_ID');
        table.string('CF_FILE_NAME', 32).notNull().unique();
        table.string('CF_FILE_PATH', 256);
        table.integer('CF_FILE_SIZE').notNull();
        table.timestamp('CF_CREATED').notNull();
        table.integer('CF_STATUS').notNull();
        table.integer('CF_STREAM').notNull();
        table.integer('CF_EXTENSION').notNull();
        table.integer('CF_RUN_ID').notNull()
          .references('RUNS.RUN_ID').withKeyName('CASTOR_FILES_RUN_ID_FK');

        table.index(['CF_RUN_ID'], 'CF_RUN_IDX');
      });

    await knex.schema.createTable('CASTOR_FILE_EVENTS',
      (table) => {
        table.increments('CFE_ID');
        table.integer('CFE_EVENT').notNull().comment('leads to TRIG_TRIGGER_NUMBER in TRIGGERS table, not using a foreign key there');
        table.integer('CFE_OFFSET').notNull();
        table.integer('CFE_SEQUENCE').notNull();
        table.integer('CFE_CF_ID').notNull()
          .references('CASTOR_FILES.CF_ID').withKeyName('CASTOR_FILE_EVENTS_CF_ID_FK');

        table.unique(['CFE_CF_ID', 'CFE_EVENT'], 'CFE_CF_EVENT_UNIQ');
      });

    /* creating relation tables */
    await knex.schema.createTable('REL_DOCS_MATERIALS',
      (table) => {
        table.integer('MAT_ID').notNull()
          .references('MATERIALS.MAT_ID').withKeyName('DOC_MAT_ID_FK');
        table.integer('DOC_ID').notNull()
          .references('DOCUMENTS.DOC_ID').withKeyName('MAT_DOC_ID_FK');

        table.primary(['MAT_ID', 'DOC_ID']);
        table.index(['DOC_ID'], 'REL_DM_DOC_IDX');
      });

    await knex.schema.createTable('REL_DOCS_DET_SETUPS',
      (table) => {
        table.integer('DET_SETUP_ID').notNull()
          .references('DETECTORS_SETUPS.DET_SETUP_ID').withKeyName('DOC_DET_SETUP_ID_FK');
        table.integer('DOC_ID').notNull()
          .references('DOCUMENTS.DOC_ID').withKeyName('DET_SETUP_DOC_ID_FK');

        table.primary(['DET_SETUP_ID', 'DOC_ID']);
        table.index(['DOC_ID'], 'REL_DDS_DOC_IDX');
      });

    await knex.schema.createTable('REL_DOCS_DETECTORS',
      (table) => {
        table.integer('DET_ID').notNull()
          .references('DETECTORS.DET_ID').withKeyName('DOC_DET_ID_FK');
        table.integer('DOC_ID').notNull()
          .references('DOCUMENTS.DOC_ID').withKeyName('DET_DOC_ID_FK');

        table.primary(['DET_ID', 'DOC_ID']);
        table.index(['DOC_ID'], 'REL_DD_DOC_IDX');
      });

    await knex.schema.createTable('REL_DETECTORS_SETUPS',
      (table) => {
        table.integer('DET_ID').notNull()
          .references('DETECTORS.DET_ID').withKeyName('SETUP_DET_ID_FK');
        table.integer('DET_SETUP_ID').notNull()
          .references('DETECTORS_SETUPS.DET_SETUP_ID').withKeyName('DET_SETUP_ID_FK');
        table.integer('DET_CONT_ID')
          .references('CONTAINERS.CONT_ID').withKeyName('DET_CONT_ID_FK');

        table.primary(['DET_SETUP_ID', 'DET_ID']);
      });

    await knex.schema.createTable('REL_MATERIALS_SETUPS',
      (table) => {
        table.integer('MAT_ID').notNull()
          .references('MATERIALS.MAT_ID').withKeyName('SETUP_MAT_ID_FK');
        table.integer('MAT_SETUP_ID').notNull()
          .references('MATERIALS_SETUPS.MAT_SETUP_ID').withKeyName('MAT_SETUP_ID_FK');
        table.integer('MAT_STATUS').notNull();
        table.integer('MAT_POSITION');

        table.primary(['MAT_SETUP_ID', 'MAT_ID']);
      });

    await knex.schema.createTable('REL_DOCS_RUNS',
      (table) => {
        table.integer('RUN_ID').notNull()
          .references('RUNS.RUN_ID').withKeyName('DOC_RUN_ID_FK');
        table.integer('DOC_ID').notNull()
          .references('DOCUMENTS.DOC_ID').withKeyName('RUN_DOC_ID_FK');

        table.primary(['RUN_ID', 'DOC_ID']);
        table.index(['DOC_ID'], 'REL_DR_DOC_IDX');
      });

    await knex.schema.createTable('REL_CONF_PARAMS',
      (table) => {
        table.integer('PARAM_ID').notNull()
          .references('DAQ_PARAMETERS.PARAM_ID').withKeyName('CON_PARAM_ID_FK');
        table.integer('CONF_ID').notNull()
          .references('CONFIGURATIONS.CONF_ID').withKeyName('PARAM_CONF_ID_FK');

        table.primary(['CONF_ID', 'PARAM_ID']);
      });

};

exports.down = async function(knex) {
  await knex.schema.dropTable('REL_CONF_PARAMS');
  await knex.schema.dropTable('REL_DOCS_RUNS');
  await knex.schema.dropTable('REL_MATERIALS_SETUPS');
  await knex.schema.dropTable('REL_DETECTORS_SETUPS');
  await knex.schema.dropTable('REL_DOCS_DET_SETUPS');
  await knex.schema.dropTable('REL_DOCS_DETECTORS');
  await knex.schema.dropTable('REL_DOCS_MATERIALS');
  await knex.schema.dropTable('CASTOR_FILE_EVENTS');
  await knex.schema.dropTable('CASTOR_FILES');
  await knex.schema.dropTable('TRIGGERS');
  await knex.schema.dropTable('CONFIGURATIONS');
  await knex.schema.dropTable('DAQ_PARAMETERS');
  await knex.schema.dropTable('DAQ_INSTRUMENTS');
  await knex.schema.dropTable('HV_PARAMETERS');
  await knex.schema.dropTable('COMMENTS');
  await knex.schema.dropTable('RUNS');
  await knex.schema.dropTable('RUN_DATA_STATUS');
  await knex.schema.dropTable('RUN_STATUS');
  await knex.schema.dropTable('SHIFTS');
  await knex.schema.dropTable('ASSOCIATIONS');
  await knex.schema.dropTable('INSTITUTES');
  await knex.schema.dropTable('USERS');
  await knex.schema.dropTable('POINTS_SHIFTS');
  await knex.schema.dropTable('SHIFT_KINDS');
  await knex.schema.dropTable('CONTAINERS');
  await knex.schema.dropTable('DETECTORS_SETUPS');
  await knex.schema.dropTable('DETECTORS');
  await knex.schema.dropTable('DOCUMENTS');
  await knex.schema.dropTable('MATERIALS_SETUPS');
  await knex.schema.dropTable('MATERIALS');
  await knex.schema.dropTable('MATERIALS_TYPE');
  await knex.schema.dropTable('EXPERIMENTAL_AREAS');
  await knex.schema.dropTable('OPERATION_PERIODS');
};
