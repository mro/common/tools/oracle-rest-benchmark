// Update with your config settings.

module.exports = {

  development: {
    client: 'sqlite3',
    debug: true,
    asyncStackTraces: true,
    useNullAsDefault: true,
    connection: {
      filename: './dev.sqlite3'
    }
  },

  staging: {
    client: 'oracledb',
    debug: false,
    asyncStackTraces: false,
    connection: {
      user: "user",
      password: "password",
      connectString: "server:port/service"
    }
  },

};
