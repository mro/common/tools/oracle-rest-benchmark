document.addEventListener('DOMContentLoaded', function () {
  $("#reloadData").on('click', loadBenchmarkResults);
  loadBenchmarkResults();
});

const loadBenchmarkResults = () => {
  $.get('/api/benchmark', (data) => {
    console.log('data', data);

    $('#BenchContainer').fadeOut().html('');

    $("#iTime").html(data.context.date);
    $("#iHostname").html(data.context.host_name);
    $("#iCpus").html(data.context.num_cpus);
    $("#iMhz").html(data.context.mhz_per_cpu);
    let scaling = data.context.cpu_scaling_enabled ? "Enabled" : "Disabled";
    $("#iScaling").html(scaling);
    let build = data.context.library_build_type.replace(/^\w/, c => c.toUpperCase());
    if(build === 'Debug')
      build = "<span class='badge badge-danger'>Debug!</span>";
    else
      build = "<span class='badge badge-info'>" + build + "</span>";

    $("#iBuild").html(build);

    let reducedData = {};

    _.forEach(data.benchmarks, (benchmark) => {
      let { tech, title, type } = parseTestName(benchmark.name);
      let bench = reducedData[title] || {};

      if(_.isEmpty(bench)) {
        bench = _.pick(benchmark, ['iterations', 'threads', 'repetitions', 'time_unit']);
        bench.title = title;
      }
      let rtPath = tech + '_' + type + '_real_time';
      let rtValue = parseFloat(benchmark.real_time.toFixed(3));
      _.set(bench, rtPath, _.concat(bench[rtPath] ? bench[rtPath] : [], [ rtValue ]));

      reducedData[title] = bench;
    });


    console.info(reducedData);


    _.forEach(reducedData, (testData) => {

      let tableInfoId = testData.title + 'Info';
      let chartId = testData.title + 'Chart';

      //Create new Test Row and append to  #BenchContainer
      $('#BenchContainer').append(
        '<div class="card text-center mb-3">' +
        '  <div class="card-header"><b>' + testData.title.toUpperCase() + '</b></div>' +
        '  <div class="card-body">' +
        '    <div class="row align-items-center">' +
        '      <div class="col-2 text-center" id="' + tableInfoId + '"></div>' +
        '      <div class="col-10 graph" id="' + chartId + '"></div>' +
        '    </div>' +
        '  </div>' +
        '</div>'
      );

      //Clone Test Indo Table
      $('.utilityBox > table').clone().appendTo('#' + tableInfoId);

      //Fill Test Indo Table Information
      $('#' + tableInfoId).find('.iterations > td').html(testData.iterations);
      $('#' + tableInfoId).find('.repetitions > td').html(testData.repetitions);
      $('#' + tableInfoId).find('.threads > td').html(testData.threads);
      $('#' + tableInfoId).find('.omean > td').html(testData.oracle_mean_real_time[0]);
      $('#' + tableInfoId).find('.omedian > td').html(testData.oracle_median_real_time[0]);
      $('#' + tableInfoId).find('.ostddev > td').html(testData.oracle_stddev_real_time[0]);
      $('#' + tableInfoId).find('.rmean > td').html(testData.rest_mean_real_time[0]);
      $('#' + tableInfoId).find('.rmedian > td').html(testData.rest_median_real_time[0]);
      $('#' + tableInfoId).find('.rstddev > td').html(testData.rest_stddev_real_time[0]);

      //Highchart
      Highcharts.chart(chartId, {
        title: { text: null },
        yAxis: {
          title: { text: 'Time (' + testData.time_unit + ')' }
        },
        xAxis: {
          title: { text: 'Repetitions' }
        },
        legend: {
          layout: 'vertical',
          align: 'right',
          verticalAlign: 'middle'
        },
        tooltip: {
          shared: true,
          crosshairs: true
        },
        plotOptions: {
          line: {
            dataLabels: {
              enabled: true
            }
          },
          series: {
            cursor: 'pointer',
            marker: {
              lineWidth: 1
            },
            pointStart: 1
          }
        },

        series: [{
          name: 'Oracle',
          data: testData.oracle_data_real_time
        }, {
          name: 'Rest',
          data: testData.rest_data_real_time
        }],
      });

    });

    $('#BenchContainer').fadeIn();

  });

};

const parseTestName = (name) => {
  // {Oracle|Rest}-{title}/iterations:{/d}/repeats:{/d}/threads:{/d}[_{mean/median/stddev}]
  // Example "Rest-get_a_specific_run/iterations:10/repeats:3/threads:5"
  // Example 2 "Oracle-get_a_specific_run/iterations:10/repeats:3/threads:5_median"

  let dashed = name.split('-');
  let tech = dashed[0].toLowerCase(); // oracle or rest
  let slashed = dashed[1].split('/');
  let title = slashed[0];
  // Check if is a mean/median/stddev or normal data
  let type = 'data'; //by default
  if(_.last(slashed).includes('_')) {
    type = _.last(_.last(slashed).split('_'));
  }

  return { tech, title, type };
};