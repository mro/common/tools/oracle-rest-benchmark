# Dependencies

```
yum install -y cmake3 make gcc-c++ git
yum-config-manager --add-repo http://mro-dev.web.cern.ch/distfiles/cc7/MRO-Common.repo
yum install -y oracle-instantclient19.5-devel 
yum install -y jsoncpp jsoncpp-devel
yum install -y libcurl libcurl-devel
```

Google Benchmark needs GCC7.

```
yum install -y 'devtoolset-7-gcc*'

source /opt/rh/devtoolset-7/enable
g++ --version | grep -q '(GCC) 7'
```

This benchmark tool needs an oracle account/connection with ntof db. 
This benchmark needs also a REST service (under `ntof_website`).

# Run

Modify accordingly a configuration inside the `conf` folder and, 
with the same credential, the `staging` part of `www/knexfile.js`. 
Then, modify the `-c` parameter inside `runBenchmark.sh` with the path of the conf you want to use.

Then just run

```
bash runBenchmark.sh
```

Benchmark results will be available at http://localhost:8081/


Todo:

- [ ] Include as submodule `ntof_website` 
- [ ] Move Migration script and seed under `ntof_website`
- [ ] Full automatize the execution
- [ ] Support Openstack
