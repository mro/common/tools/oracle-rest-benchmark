#!/bin/bash

# exit when any command fails
set -e

source /opt/rh/devtoolset-7/enable
g++ --version | grep -q '(GCC) 7'

rm -rf build && mkdir build && cd build
cmake3 .. -DCMAKE_BUILD_TYPE=Release
make -j4

cd ..

cd www

npm install
export PATH="./node_modules/.bin:$PATH"
knex migrate:latest --env staging
knex seed:run --env staging

nohup node index.js > nodeServer.log &
echo $! > nodews_pid
sleep 1

cd ..

./build/src/OracleRestApiBench \
    --benchmark_out=./www/data/BenchmarkResults.json \
    --benchmark_out_format=json \
    -c ./conf/conf.json

printf "\n Done. Benchmark results are available at http://localhost:8080/ \n"

for (( ; ; ))
do
  printf "\n"
  read -r -n 1 -p "Press 'q' to shutdown the Webserver: " mainmenuinput
  if [ "$mainmenuinput" = "q" ]; then
    # shellcheck disable=SC2046
    # shellcheck disable=SC2006
    kill -9 `cat www/nodews_pid`;
    rm 'www/nodews_pid';
    break;
  fi
done

exit